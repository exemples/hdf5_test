#include <stdio.h>
#include "Hyperslab_by_chunk.h"

int
main (int argc, char **argv)
{
    /*
     * Initialize MPI
     */
    MPI_Init(&argc, &argv);

    cree_fichier(H5FILE_NAME);

    MPI_Finalize();

    return 0;
}

