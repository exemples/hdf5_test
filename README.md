Cet exemple est tiré de la doc HDF5 en ligne:
https://support.hdfgroup.org/HDF5/Tutor/phypechk.html
attention, à la date de août 24, il y a plusieurs sites du HDF, le site officiel n'a plus cette doc en ligne


# Compilation
il faut du HDF5 parallèle, et un cmake assez récent

`
git clone git@gitlab.fresnel.fr:exemples/hdf5_test.git
`

```
env CC=/home/me/LIBRARY_PARA/LIBRARIES/hdf5-1.12.3/bin/h5pcc /home/me/.local/share/JetBrains/Toolbox/apps/clion-2/bin/cmake/linux/x64/bin/cmake -DCMAKE_C_FLAGS="-g -DDEBUG -DH5_HAVE_PARALLEL" -DHDF5_DIR=${HOME}/LIBRARY_PARA/LIBRARIES/hdf5-1.12.3  .. 
```

`
make VERBOSE=1
`

`
mpirun -np 4 ./Hyperslab_by_chunk 
`

qui crée le fichier h5:

```
(base) me@nessie:~/projets/tmp/hdf5_test/build$ h5dump SDS_chnk.h5 
HDF5 "SDS_chnk.h5" {
GROUP "/" {
   DATASET "IntArray" {
      DATATYPE  H5T_STD_I32LE
      DATASPACE  SIMPLE { ( 8, 4 ) / ( 8, 4 ) }
      DATA {
      (0,0): 1, 1, 2, 2,
      (1,0): 1, 1, 2, 2,
      (2,0): 1, 1, 2, 2,
      (3,0): 1, 1, 2, 2,
      (4,0): 3, 3, 4, 4,
      (5,0): 3, 3, 4, 4,
      (6,0): 3, 3, 4, 4,
      (7,0): 3, 3, 4, 4
      }
   }
}
}
```