//
// Created by me on 08/08/24.
//

#ifndef HDF5_TEST_HYPERSLAB_BY_CHUNK_H
#define HDF5_TEST_HYPERSLAB_BY_CHUNK_H

#define H5FILE_NAME     "SDS_chnk.h5"
#define DATASETNAME 	"IntArray"
#define NX     8                      /* dataset dimensions */
#define NY     4
#define CH_NX  4                      /* chunk dimensions */
#define CH_NY  2
#define RANK   2


int cree_fichier(char *f);


#endif //HDF5_TEST_HYPERSLAB_BY_CHUNK_H
